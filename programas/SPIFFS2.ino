#include <SPIFFS.h>

void setup() {
     Serial.begin(115200);
 
     if (!SPIFFS.begin(true)) {
         Serial.println("Ocurrio error al intentar montar SPIFFS");
         return;
     }

     String path="/index.html";
     if(SPIFFS.exists(path)){
          Serial.println("Si existe el archivo");
          File archivo=SPIFFS.open(path,"r");
          if(!archivo){
               Serial.println("No se puede abrir el archivo para leer");
               return;
          }

          Serial.println("contenido del documento:");

          while(archivo.available()){

                Serial.write(archivo.read());
          }
     }
}

void loop() {

}
