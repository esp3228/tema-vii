
#include <ESP32Servo.h>
#include "WiFi.h"
#include "WebServer.h"


const char * ssid="hercab2k20";
const char * passwd="AxVa9295";

Servo myservo;  // Crear objeto servo para gestionar un servomotor

WebServer server(80);  // Crear servidor que escucha peticiones por el puerto 80

String pagina="<!DOCTYPE html>"
              "<html>"
              "<head>"
              "<title>ESP32 </title>"
              "</head>"
              "<body>"
              "<center><h1>Formulario ESP32</h1>"
              "<hr/>"
              "<p><h3>Moviendo servomotor</h3>"
              "<form action='/envio' method='post'>"
              "<label>Mueve el slice para seleccionar grados (0-180):</label>"
              "<input type='range' value='0'  min='0' max='180' name='numero'><br/>"
              "<input type='submit' value='Enviar'>"
              "</form>"
              "</p>"
              "</center>"
              "</body>"
              "</html>";




void localIP(){
  Serial.print("IP asignada:");
  Serial.println(WiFi.localIP());
}

void wifiInit(){
    int intentos =0;
    // Establece modo de operación Estación y desconecta el modulo WiFi si estuviera conectado
    WiFi.mode(WIFI_STA);
    WiFi.disconnect();
    // Conexión a red específica.
    WiFi.begin(ssid,passwd);
    Serial.print("Conectando .");
    while(WiFi.status() != WL_CONNECTED && intentos++!=30){
      Serial.print(".");
      delay(1000);
    }
    Serial.println();
    if(WiFi.status()== WL_CONNECTED){
        Serial.println("Conexión completada correctamente!!");
        localIP();
    }
    else{
        Serial.print("Tiempo sobrepasado para conectarse a:");
        Serial.println(ssid);
    }
    
}



void gestorRaiz(){
      server.send(200,"text/html",pagina);
}


void gestorNoLocalizado(){
  String message = "Recurso Solicitado\n\n";
  message += "URI: ";
  message += server.uri();
  message += "\nMetodo: ";
  message += (server.method() == HTTP_GET) ? "GET" : "POST";
  message += "\nArgumentos: ";
  message += server.args();
  message += "\n";

  for (uint8_t i = 0; i < server.args(); i++) {
    message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
  }

  server.send(404, "text/plain", message);
}

void gestorFormulario(){
  int pos=(server.arg(0)).toInt();
  myservo.write(pos);              
  delay(15); 
  server.send(200,"text/html",pagina);
}



void webInit(){
  server.on("/",gestorRaiz);
  server.on("/envio",gestorFormulario);
  server.onNotFound(gestorNoLocalizado);
  server.begin();
  Serial.println("Servidor web ha sido iniciado");
}


void setup() {
  myservo.attach(13); // Se selecciona el pin 13 para gestionar servomotor
  Serial.begin(115200);
    delay(100);
    wifiInit();
    if(WiFi.status()==WL_CONNECTED)
        webInit(); 
}


void loop() {
  server.handleClient();
  delay(2);                       

}
